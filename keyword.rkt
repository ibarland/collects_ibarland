#lang racket
(provide (all-defined-out))
         
(module+ test (require rackunit))


#;(require mischief/keyword)
(define keyword->symbol (compose string->symbol keyword->string))


; convert a list of keywords and their respective values to an (improper) a-list,
; also converting the keywords to symbols.
(define (keywords->a-list kws kw-args)
  (map list (map keyword->symbol kws) kw-args))

(module+ test
  (check-equal? (keyword->symbol '#:hi) 'hi)
  (check-equal? (keywords->a-list '(#:hi #:bye) '(3 "for now"))
                '((hi 3) (bye "for now")))
  (check-equal? (keywords->a-list '() '()) '())
  )
