#lang racket

(provide (all-defined-out)
         read-csv-file)

;; TODO: if reading tsv, and there are fewer columns than the header would indicate, init them to "" (?)


(require 2htdp/batch-io)
(require ibarland/common ibarland/abbrevs)

(module+ test (require rackunit))


(define (trim-if-string val) (tweak val string? string-trim))

; #:trim?   string-trim each string?
(define (read-tsv-file tsv-file #:ignore-first-line [ignore-first-line? #f] #:trim? [trim? #f] #:verbose? [verbose? #f])
  (unless (regexp-match #px"\\.(tsv|tab)$" tsv-file)
    (error 'read-tsv-file "filename ~v doesn't end in '.tsv' or '.tab'" tsv-file))
  (define trimmer (if trim? string-trim identity))
  (define table ((if ignore-first-line? rest identity)
                 (map (λ(line) (regexp-split #px"\t" line))
                      (file->lines tsv-file #:mode 'text))))
  (when verbose? (printf "~v rows read from ~a.~n" (length table) tsv-file))
  table
  )

(define (write-tsv-file table tsv-file #:exists [exists 'error] #:verbose? [verbose? #f])
  (unless (regexp-match #px"\\.(tsv|tab)$" tsv-file)
    (error 'write-tsv-file "filename ~v doesn't end in '.tsv' or '.tab'" tsv-file))
  (with-output-to-file tsv-file #:exists exists  #:mode 'text
    (λ() (for-each (λ(row) (displayln (apply str+ (add-between row "\t"))))
                   table)))
  (when verbose? (printf "~v lines saved to ~a.~n" (length table) tsv-file))
  )

;;;;;;;;; a bunch of helper quasi-database functions, for dealing with info from a csv file
;;;;;;;;; (Hmm, surely there's an existing racket library for this?)

; data def'n: a "column-name" is a string
; data def'n: a "header" is a (listof column-name)
; data def'n: a "row" is a (listof string?) where each entry is one column of the row.
;
; data def'n: a "table" is what read-csv-file returns, when the first row was column-names.
;    Namely, it's (cons header (listof row))
;    where the header and each row all have the same length.
;
(define column-names first) ; given a table with headers, return just the column-names
(define rows rest)          ; given a table with headers, return just the data-rows
(define (num-rows table) (length (rows table)))

; column-name header table ->  (-> row string)
;   return a getter for column-name `target-col-name`, to be applied to  individual rows.
; E.g. `(column-getter "Coach" people)` might return `(λ(row) (list-ref row 4))`
;      (a function that extracts the fifth element from a list, a.k.a. `fifth`).
;   Raises an error if that column doesn't exist.
;
(define (column-getter target-col-name table)
  (define headers (column-names table))
  (or (for/first {[i (length headers)] #:when (string-ci=? (list-ref headers i) target-col-name)}
        (λ(row) (list-ref row i)))
      (error 'column-getter "No column name ~v among ~v." target-col-name headers)))

; string, table, row -> string
; convenience function: apply the column-getter to a row.
;;;; TODO: make the table the *last* arg, to match other functions below.
(define (column-get target-col-name table row)
  ((column-getter target-col-name table) row))


; string, table, row, string -> string
; return the field, but if it's "" then return `default-if-field-empty` instead.
;
(define (column-get-or target-col-name table row [default-if-field-empty #f])
  (tweak (column-get target-col-name table row)
         (curry string=? "")
         default-if-field-empty))


; get 1 column from a table, as a list (w/ repetitions)
(define (select1* col-name table)
  (map (column-getter col-name table) (rows table)))

; get 1 column from a table, as a set (no repetitions)
(define (select1 col-name table) (remove-duplicates (select1* col-name table)))

(module+ test
  (check-equal? (select1* "b" '(("a" "b" "c") (222 3 444) (55 5 555) (44 4 444)))
                '(3 5 4)))

; filter a table, based on `keep?` applied to each row.
; (Just like `filter` except that it includes the first row (headers) in the result.)
;
(define (where keep? table)
  (cons (column-names table)
        (filter keep? (rows table))))


(module+ test
  (define-simple-check (check-equal-tables? a b)
    (and (equal? (column-names a) (column-names b))
         (equal? (list->set (rows a)) (list->set (rows b)))))

  ; make sure we don't test order of rows
  (check-equal-tables? '(("name" "title" "height")
                         ("Ian" doc 195)
                         ("Mer" maestro 180))
                       '(("name" "title" "height")
                         ("Mer" maestro 180)
                         ("Ian" doc 195)))
  ; BUG: we should allow transposing column-names
  #;(check-equal-tables? '(("name" "title" "height")
                           ("Ian" doc 195))
                         '(("name" "height" "title")
                           ("Ian" 180 doc)))
  )


(module+ test
  (check-equal? (where (λ(row) (string=? "nope" (second row)))
                       '(("stuff" "site" "name" "other")
                         (3 "nope" "bla" 4)
                         (5 "yep!" "NaNNaNNah")
                         (6 "nope" "blah" 7)))
                '(("stuff" "site" "name" "other")
                  (3 "nope" "bla" 4)
                  (6 "nope" "blah" 7)))
  )
  

; filter a table, based on the `col-name`s value being string=? to `val`, in `table`.
(define (where= col-name val table)
  (where (λ(row) (string=? val (column-get col-name table row))) table))

(module+ test
  (check-equal? (where= "site" "nope" '(("stuff" "site" "name" "other")
                                        (3 "nope" "bla" 4)
                                        (5 "yep!" "NaNNaNNah")
                                        (6 "nope" "blah" 7)))
                '(("stuff" "site" "name" "other")
                  (3 "nope" "bla" 4)
                  (6 "nope" "blah" 7)))
  )

; select one row of a table, given a primary key (PK).
; `col-name` is the name of the PK column; `val` is the target value to look up in `table`.
;
(define (select-pk col-name val table)
  (define one-row (rows (where= col-name val table)))
  (unless (= 1 (length one-row))
    (error 'select-pk "non-pk: selecting ~v=~v in ~v, got ~v." col-name val (column-names table) one-row))
  (first one-row))

(module+ test
  (check-equal? (select-pk "name" "NaNNaNNah" '(("stuff" "site" "name" "other")
                                                (3 "nope" "bla" 4)
                                                (5 "yep!" "NaNNaNNah")
                                                (6 "naw" "blah" 7)))
                '(5 "yep!" "NaNNaNNah"))
  (check-exn exn:fail? (λ() (select-pk "site" "nope" '(("stuff" "site" "name" "other")
                                                       (3 "nope" "bla" 4)
                                                       (5 "yep!" "NaNNaNNah")
                                                       (6 "nope" "blah" 7)))))
  )
  


; given a primary key, look up another column in that entry:
(define (lookup col-target col-pk col-pk-value table)
  (column-get col-target table (select-pk col-pk col-pk-value table)))

(module+ test
  (check-equal? (lookup "site" "name" "NaNNaNNah" '(("stuff" "site" "name" "other") (3 "nope" "bla" 4) (5 "yep!" "NaNNaNNah") (6 "nope" "blah" 7)))
                "yep!")
  (check-exn exn:fail? (λ() (lookup "site" "name" "blah" '(("stuff" "site" "name" "other") (3 "nope" "blah" 4) (5 "yep!" "NaNNaNNah") (6 "nope" "blah" 7)))))
  )



; update-where= -- return a function that will
;   replace any `target` with `replacement`
;   in the column named `col-name` in `table`.
; given a single row from that table.
;
; TODO: make this change the table, not return a single-row-processing-function.
;
(define (update-where= col-name table target replacement)
  (define heads (column-names table))
  (define (helper headings row)
    (cond [(empty? headings) (error 'replace-column-if "column-name ~v not found in ~v." col-name heads)]
          [(string=? (first headings) col-name) (cons (if (string=? target (first row)) replacement (first row))
                                                      (rest row))]
          [else (cons (first row) (helper (rest headings) (rest row)))]))

  (λ(row) (helper heads row)))

(module+ test
  (check-equal? ((update-where= "site" '(("stuff" "site" "name" "other") (3 "nope" "bla" 4) (5 "yep!" "NaNNaNNah") (6 "nope" "blah" 7)) "nope" "nawww")
                 '(3 "nope" "bla" 4))
                '(3 "nawww" "bla" 4))
  )

; Project the table down to the desired columns.
; (list-of string) table -> table
; A table is (morally) a set of rows, so no duplicate rows, and row-order not preserved.
;
(define (project headers-to-keep table)
  ; given a bunch of col-names, turn that into a bunch of getters:
  (define cols-to-keep (map (λ(nm) (column-getter nm table)) headers-to-keep))
  (cons headers-to-keep  ; the headers of the resulting table
        (remove-duplicates (for/list {[r (rows table)]}
                                     (for/list {[getter cols-to-keep]} ; or: (map (λ(getter) (getter r)) cols-to-keep)
                                               (getter r))))))

(define (remove-columns headers-to-drop table)
  (project (remove* headers-to-drop (column-names table)) table))
(define (remove-column header-to-drop table)
  (remove-columns (list header-to-drop) table))


(module+ test
  
  (check-equal-tables? (project '() '(("stuff" "site" "name" "other")
                                      (3 "nope" "bla" 4)
                                      (5 "yep!" "NaNNaNNah")
                                      (6 "nope" "blah" 7)))
                       '(()  ; the headers
                         ()  ; weird: the one unique row-with-zero-columns
                         ))
  (check-equal-tables? (project '("stuff") '(("stuff" "site" "name" "other")
                                             (3 "nope" "bla" 4)
                                             (5 "yep!" "NaNNaNNah")
                                             (6 "nope" "blah" 7)))
                       '(("stuff") (3) (5) (6)))
  (check-equal-tables? (project '("stuff") '(("stuff" "site" "name" "other")
                                             (3 "nope" "bla" 4)
                                             (5 "yep!" "NaNNaNNah")
                                             (3 "another" "three!" 7)
                                             (6 "nope" "blah" 7)))
                       '(("stuff") (3) (5) (6)))
  (check-equal-tables? (project '("stuff") '(("stuff" "site" "name" "other")
                                             ; No rows at all
                                             ))
                       '(("stuff")))
  (check-equal-tables? (project '() '(("stuff" "site" "name" "other")
                                      ; No rows at all
                                      ))
                       '(()))

  (check-equal-tables? (project '("site" "name") '(("stuff" "site" "name" "other")
                                                   (3 "nope" "bla" 4)
                                                   (5 "yep!" "NaNNaNNah")
                                                   ("another" "nope" "bla" "don't keep duplicates!")
                                                   (6 "nope" "blah" 7)))
                       '(("site" "name")
                         ("nope" "bla")
                         ("yep!" "NaNNaNNah")
                         ("nope" "blah")))

  (check-equal-tables? (remove-columns '("stuff" "other") '(("stuff" "site" "name" "other")
                                                            (3 "nope" "bla" 4)
                                                            (5 "yep!" "NaNNaNNah")
                                                            ("another" "nope" "bla" "don't keep duplicates!")
                                                            (6 "nope" "blah" 7)))
                       '(("site" "name")
                         ("nope" "bla")
                         ("yep!" "NaNNaNNah")
                         ("nope" "blah")))
  )


; sort-by : string, table -> table
; sort a table by the indicated column
(define (sort-by col table)
  (cons (column-names table)
        (sort (rows table) string<? #:key (column-getter col table))))


; result has as many rows as t1
(define (join-pk t1 t2 k1 [k2 k1])
  ;;;(when (not (= (length t1) (length t2))) (error 'join-pk "unequal lengths (~v,~v)" (length (rows t1)) (length (rows t2))))
  (define header1 (column-names t1))
  (define header2 (column-names t2))
  (when (not (member k1 header1)) (error 'join-pk "no key ~v in first table ~v" k1 header1))
  (when (not (member k2 header2)) (error 'join-pk "no key ~v in second table ~v" k2 header2))
  (define common-headers (set-subtract (set-intersect (list->set header1) (list->set header2))
                                       (if (equal? k1 k2) (set k1) (set))))
  (when (not (set-empty? common-headers)) (error 'join-pk "duplicate columns ~v" common-headers)) 
  
  (define get1 (column-getter k1 t1))
  (cons (append header1 header2)
        (for/list {[r1 (rows t1)]}
                  (append r1 (select-pk k2 (get1 r1) t2)))))

(module+ test
  (define tab1  '(("a" "b" "c") ("1" "2" "3") ("4" "5" "6") ("1" "8" "9")))
  (define tab2  '(("a" "d" "f") ("1" "x" "y") ("4" "b" "c")))
  (define tab2Z '(("Z" "d" "f") ("1" "x" "y") ("4" "b" "c"))) ; tab2, but different col-name to join on.
  (check-equal? (join-pk tab1 tab2  "a" "a")
                '(("a" "b" "c" "a" "d" "f") ("1" "2" "3" "1" "x" "y") ("4" "5" "6" "4" "b" "c") ("1" "8" "9" "1" "x" "y"))) ; BUG repeated col-name & val
  (check-equal? (join-pk tab1 tab2  "a") ; use default k2
                '(("a" "b" "c" "a" "d" "f") ("1" "2" "3" "1" "x" "y") ("4" "5" "6" "4" "b" "c") ("1" "8" "9" "1" "x" "y"))) ; BUG repeated col-name & val

  (check-exn #px"[Dd]uplicate columns" (λ() (join-pk tab1 tab1 "a" "a"))) ; duplicate columns
  (check-exn #px"[Nn]o key" (λ() (join-pk tab1 tab2 "zz" "a"))) ; no key in first table
  (check-exn #px"[Nn]o key" (λ() (join-pk tab1 tab2 "a" "zz"))) ; no key in second table
  (check-exn #px"[Nn]o key" (λ() (join-pk tab1 tab2Z "a")
                              '(("a" "b" "c" "Z" "d" "f") ("1" "2" "3" "1" "x" "y") ("4" "5" "6" "4" "b" "c"))))
  ;;;(check-exn #px"[Uu]nequal lengths" (λ() (join-pk tab1 (append tab2 '(("*" "*" "*"))) "a")))
  
  )


; return the join of pk-table and fk-table,
; where the two tables share one exactly column-name in common,
; and it's a PK in the former and a FK in the latter.
; The result has as many rows as fk-table.  ; WAIT is this what I want? see `find-teams-<3members.rkt` 2021-contest, l.44
;
(define (join-on-1fk fk-table pk-table)
  (define fk-h (column-names fk-table)) ; headers for table1
  (define pk-h (column-names pk-table))
  (define join-keys (for/list {[h (set-intersect (list->set fk-h) (list->set pk-h))]} h))
  (assert (empty? (rest join-keys))) ; make sure exactly one column-name in column
  (define join-key (first join-keys))
  (define get-join-col2 (column-getter join-key pk-table))
  (define pk-table-hashed (for/hash {[r (rows pk-table)]} (values (get-join-col2 r) r)))
  (unless (= (hash-count pk-table-hashed) (length (rows pk-table)))
    (error 'join-on-1fk "not a primary key? ~v occurs ~v times, not ~v, in ~v" join-key (length (rows pk-table)) (hash-count pk-table-hashed) pk-table)) ; make sure it was a pk (no duplicates)
  ;(define join-key-index1 (index-of fk-h join-key))
  (define join-key-pk-index (index-of pk-h join-key))
  (define get-join-col1 (column-getter join-key fk-table))

  (define h (append fk-h (drop-at pk-h join-key-pk-index)))
  (define joined-rows (for/list {[r-fk (rows fk-table)]}
                         (define r-pk (hash-ref pk-table-hashed (get-join-col1 r-fk)))
                         (append r-fk (drop-at r-pk join-key-pk-index))))
  (cons h joined-rows))


(define (join-on-1fk* . tables)
  ; TODO does this make sense, considering each successive table first a pk-table, and then a fk-table???
  ; I think we want foldr.   see `find-teams-<3members.rkt` 2021-contest, l.44
  ; Separately: better to have the caller provide the pk table first, then the fks?
  (foldl (λ(a b) (join-on-1fk b a)) (first tables) (rest tables)))


(module+ test
  (define t1 '(("a" "b" "c") (1 2 3) (4 5 6) (7 8 9) (0 11 12)))
  (define t2 '(("a" "x" "y") (1 m n) (1 p q) (7 r s)))
  (define t3 '(("x" "c" "y") (a 6 b) (c 9 d) (e 9 f)))
  (check-equal? (join-on-1fk t2 t1)
               '(("a" "x" "y" "b" "c") (1 m n 2 3) (1 p q 2 3) (7 r s 8 9)))
  
(check-equal? (join-on-1fk t3 t1)
               '(("x" "c" "y" "a" "b") (a 6 b 4 5) (c 9 d 7 8) (e 9 f 7 8)))

  (check-equal? (join-on-1fk* t1) t1)
  (check-equal? (join-on-1fk* t2 t1) (join-on-1fk t2 t1))
  (check-equal? (join-on-1fk* t3 t1) (join-on-1fk t3 t1))
  (check-equal? (join-on-1fk* t3 t1 '(("b" "z") (5 55) (8 88) (9 99))) 
               '(("x" "c" "y" "a" "b" "z") (a 6 b 4 5 55) (c 9 d 7 8 88) (e 9 f 7 8 88)))
  )

; Just change the name of a column.
(define (rename-column old new tab)
  (cons (replace1 old new (column-names tab))
        (rows tab)))
(define (rename-columns . olds-news-tab)
  (when (even? (length olds-news-tab))
    (raise-argument-error 'rename-columns "a list of odd length (old+new column-name pairs, and the table)" olds-news-tab))
  (define-values (olds-new tab) (split-at olds-news-tab (sub1 (length olds-news-tab))))
  (let loop {[olds-news (drop-right olds-news-tab 1)]
             [tab (last olds-news-tab)]
             }
    (cond [(empty? olds-news) tab]
          [else (loop (drop olds-news 2) (rename-column (first olds-news) (second olds-news) tab))])))

(module+ test
  (check-equal? (rename-column "b" "harumph" t1)
                '(("a" "harumph" "c") (1 2 3) (4 5 6) (7 8 9) (0 11 12)))
  (check-equal? (rename-columns 'a 'aa 'b 'bb '((a z c b e)))
                '((aa z c bb e))))
