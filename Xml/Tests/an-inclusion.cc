#include <iostream>   // For cin, cout (standard input/output).
#include <fstream>    // For the types ifstream, ofstream.
#include <string>     // For type string, which is really a class (not a built-in).


int main( int argc, char* args[] ) {
  const int maxlines = 5;  /* How many lines to copy between files. */

  /* Exam our command-line arguments.  args[0] is the name of the program. */
  if ((argc != 2) || (args[1][0] == '-')) {
    cout << "Usage: " << args[0] << " [base-filename]" << endl;
    cout << "This will read " << maxlines << " lines from "
         << "[base-filename]-in.txt, and write to [base-filename]-out.txt."
         << endl;
    cout << "Each line of the input file should have a number "
         << "followed by anything" << endl;
    exit(1);  // Return an error code.
    }

  string basefilename = args[1];
  cout << "Reading from/to " << basefilename << "-in/out.txt" << endl;

  // Open the input/output files:
  string fnameOut = basefilename + "-out.txt";
  string fnameIn(basefilename + "-in.txt");   // An equivalent way to initialize a string.
  ifstream in(fnameIn.c_str());    // Declare & Initialize input-file-stream.
  if (in.fail()) {
      cerr << "File " << fnameIn << " not found (?)" << endl;
      exit(2);
      }

  ofstream out(fnameOut.c_str());  // Annoyingly, we must convert the string
    // into a c-style-string (calling method c_str), since the fstream
    // constructors don't recognize the fancy STL string class.
  if (out.fail()) {
      cerr << "File " << fnameOut << " couldn't be opened for output(?)" << endl;
      exit(2);
      }


  int n;
  string msg; // This is a fancy STL string; no need to pre-allocate 
              // some fixed amount of space, like old c-style strings (char*).

  // Okay, the heart: a loop to read a line and write it to output file,
  // echoing to stdout as well.
  //
  for ( int i = 0;  (i < maxlines);  i++ ) {
    in >> n;    // Read one number from in.
    /*in >> msg;*/  // This would only read one word of 'in'
                    // (a word, since msg is of type string).

    /* BE CAREFUL of "in >> n": if 'in' doesn't begin with a number,
     * then not only is n unchanged, but all further reads from 'in' will fail?!
     * A general technique: always read files a line at a time, and then
     * you can read input from the string rather than the file; if the
     * string has the wrong format, it's easier to recover:
     */
    /* Check whether file has had an error condition: */
    if (in.fail()) { cout << "Uh-oh -- " << fnameIn << " line#" << i
                         <<  "doesnt' start with a number?? " << endl; }

    getline(in, msg);
    if (in.eof()) break;  // Leave loop abruptly.  Yuck.

    cout << "Line " << i << " (w/ #" << n << "): " << msg << endl;
    out << n << msg << endl;  
    }


  return 0;  // By convention "main" should return 0 to indicate 
             // successful run back to the OS.
  }
