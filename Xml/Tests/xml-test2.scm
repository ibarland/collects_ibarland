(require (lib "xml.ss" "Ian" "Xml"))
(require (lib "util.ss" "Ian"))
(require (lib "xml.ss" "xml"))
(xexpr->string '(em {@} "hi there " amp (em {@} "everything")))


(process `(em {} (scheme {} "\"hello " amp " bye\"")))
(process `(em {} (scheme {} "\"3 &lt; 4\""))) ; BUG: string-constant processed.
(process `(em {} (scheme {} "(if (" lt " 3 4) 'yeah 'hmm)")))

#|   Some notes on unexpected behavior/bugs when using scheme tags:
<scheme>
(make-truth-table
  '(pa pb pc)
  `{[(if {} pa (if {} pb pc))  ,(lambda (a b c) (implies a (implies b c)))]
    [(if {} (if {} pa pb) pc)  ,(lambda (a b c) (implies (implies a b) c))]})
</scheme>
|#
  ; Note: Usually the column-header would just be a string,
  ; but here we want to include some tags and entitites, not just
  ; a simple string.  We must use the scheme representation, since we're
  ; inside scheme tags.
  ; Note: If your scheme code wants to use a less-than function, you must write "&lt;".
  ;       There are bugs, about trying to include entities in scheme-string-constants.
  ;       Don't do it.
  ; Note: "comment" tags inside "scheme" tags may not get stripped out.
  ; Furthermore, in the future, any "comment" and "scheme" tags returned by 
  ;       this scheme code may not be processed.

(define gallery
        '(gallery {} (title {} "Me at the Britney Spears Concert")
                     (picture {[filename "pict01.jpg"]}
                              (caption {} "Waiting in line for a Pepsi."))
                     (picture {[filename "pict07.jpg"]}
                              (caption {} "Waiting in line for " (em {} "another") " Pepsi."))
                     (picture {[filename "pict19.jpg"]}
                              (caption {} "Waiting in line for the bathroom."))))
  
#|
(xexpr->file "sample-gallery.xml"
             gallery-xexpr
             false ;overwrite
             true  ;verbose
             )
|#

(test
 (map (lambda (fn) (map fn (list empty 47 'hello "hello-str" gallery '(leer ()) '(taggie "hi" (it "you")))))
      (list entity-xexpr? numeric-entity-xexpr? nonnumeric-entity-xexpr? tagged-xexpr? xexpr?))
 = 
 (list
  (list false true true false false false false)
  (list false true false false false false false)
  (list false false true false false false false)
  (list false false false false true true false)
  (list false true true true true true false)))

(define xtest1 '(kablooey {[nana "nono"]} "this is " (em {} "not" (nana {} "selected")) 
                                          "but " (nana {} "one")
                                          "and also " (nana {} "two") "is."))
(define xtest2 '(smaller {[purpose "test"]} "Ex. w/ only " (nesting {} "one nesting.")))

(test (make-xexpr 'hi '{[ho "hum"]} '("there")) = '(hi ([ho "hum"]) "there"))
(test (make-xexpr (xexpr-tag gallery) (xexpr-attrs gallery) (xexpr-body gallery)) = gallery)
(test (xexpr-tag   xtest2) = 'smaller)
(test (xexpr-attrs xtest2) = '{[purpose "test"]})
(test (xexpr-body  xtest2) = '("Ex. w/ only " (nesting {} "one nesting.")))

(test (xexpr-is-a? 'gallery gallery)  = true)
(test (xexpr-is-a? 'gallery '(gal {} "lon")) = false)
(test (xexpr-is-a? 'gallery 'gallery) = false)
(test (xexpr-is-a? 'gallery "gallery furniture, saves you money!") =     false)




(test (filter-tag   'nana xtest1) = '((nana {} "one") (nana {} "two")))
(test (filter-tag01 'kiwi xtest1) = false)
(test (filter-tag1  'em xtest1)   = '(em {} "not" (nana {} "selected")))
(test (filter-tag01 'em xtest1)   = '(em {} "not" (nana {} "selected")))

(test (replace-tag xtest2 'size-challenged)
      = '(size-challenged {[purpose "test"]} "Ex. w/ only " (nesting {} "one nesting.")))
(test (replace-attrs xtest2 '{[porpoise "nets"] [dolphin "tuna"]})
      = '(smaller {[porpoise "nets"] [dolphin "tuna"]} "Ex. w/ only " (nesting {} "one nesting.")))
(test (replace-body xtest2 '("a" (tiny {} " eensy ") "change"))
      = '(smaller {[purpose "test"]} "a" (tiny {} " eensy ") "change"))
;(test ((make-body-replacer (lambda (x) `("a" (tiny {} " eensy ") "change"))) xtest2)
;      = '(smaller {[purpose "test"]} "a" (tiny {} " eensy ") "change" ))
;(test ((make-body-replacer (lambda (x) `("a" (tiny {} " eensy ") "change" ,@(xexpr-body x)))) xtest2)
;      = '(smaller {[purpose "test"]} "a" (tiny {} " eensy ") "change" "Ex. w/ only " (nesting {} "one nesting.") ))
(test (insert-attr 'porpoise "nets" xtest2)
      = '(smaller {[purpose "test"] [porpoise "nets"]} "Ex. w/ only " (nesting {} "one nesting.")))

(define add-hvho (make-attr-inserter 'heave "ho"))
(test (add-hvho xtest2)
      = '(smaller {[purpose "test"] [heave "ho"]} "Ex. w/ only " (nesting {} "one nesting.")))



(test (insert-attrs '{[hi "there"] [big "boy"]} '(donut {} "forever"))
      = `(donut {[hi "there"] [big "boy"]} "forever"))
(test (insert-attrs '{[hi "there"] [big "boy"]} '(donut {[z "are"]} "forever"))
      = `(donut {[z "are"] [hi "there"] [big "boy"]} "forever"))


(test (xexpr-synopsis 'hello false)  = "")
(test (xexpr-synopsis 'hello true)   = "&hello;")
(test (xexpr-synopsis 20 false)  = "")
(test (xexpr-synopsis 20 true)   = "&20;")
(test (xexpr-synopsis "hello" false) = "")
(test (xexpr-synopsis "hello" true)  = "he..")
(test (xexpr-synopsis '(hello {}) false) = "<hello/>")
(test (xexpr-synopsis '(hello {}) true)  = "<hello[0]/>")
(test (xexpr-synopsis '(hello {} "howdy") false) = "<hello/>")
(test (xexpr-synopsis '(hello {} "howdy") true)  = "<hello[1]/>")
(test (xexpr-synopsis '(hello {} "howdy" all) false) = "<hello/>")
(test (xexpr-synopsis '(hello {} "howdy" all) true)  = "<hello[2]/>")
(test (xexpr-synopsis '(comment {} "lala" (em {} "ho!")) false) = "<comment/>")
(test (xexpr-synopsis '(comment {} "lala" (em {} "ho!")) true)  = "<comment[2]/>")


"Testing 'process'"

; "comment" is already registered by xml.ss itself.
(test (process "hello") = '("hello"))
(test (process 'hello)  = '(hello))
(test (process 'ldots)  = '("..."))

(test (process '(hi {})) = (list '(hi {})))
(test (process '(comment {} "lala" (em {} "ho!"))) = (list ))
(test (process '(html {} "hello"))                 = (list '(html {} "hello")))
(test (process '(html {} (comment {} "lala")))     = (list '(html {})))
(test (process '(html {} (comment {} "lala") "hello"))    = (list '(html {} "hello")))
(test (process '(html {} "hello" (comment {} "lala")))    = (list '(html {} "hello")))
(test (process '(html {} (comment {} "lala") "hell" "o")) = (list '(html {} "hell" "o")))

       
(test
 (process '(html {} (head {} "hi there" (comment {} "bye all")) (comment {} "ignore" ldots) (body {} (h3 {} "hi!" ldots "?"))))
 =
 '((html {} (head {} "hi there") (body {} (h3 {} "hi!" "..." "?")))))


(test (process '(comment {} 2 3 4))
      = 
      empty)



"misc functions"

; table-data:
; Useful as <table><scheme>(table-data ..)</scheme></table>.
;
(test (table-data `()) = `(table-data {}))
(test (table-data `((a b c) (d e f)))
      = `(table-data {} (row {} (entry {} a) (entry {} b) (entry {} c))
                        (row {} (entry {} d) (entry {} e) (entry {} f))))
(test (table-data `((a (entry {} b1 " and " b2)) ("c" (fun {} "d"))))
      = `(table-data {} (row {} (entry {} a) (entry {} b1 " and " b2))
                        (row {} (entry {} "c") (entry {} (fun {} "d")))))
(test (table-data '((a b c)) false 2)
      = `(table-data {[header-cols "2"]}
                     (row {} (entry {} a) (entry {} b) (entry {} c))))
(test (table-data '((a b c)) 1 2)
      = `(table-data {[header-rows "1"] [header-cols "2"]}
                     (row {} (entry {} a) (entry {} b) (entry {} c))))

