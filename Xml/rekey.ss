(module rekey mzscheme
  (provide
     ; Temporary provides, for testing
   )
  (require (lib "xml.ss" "Ian" "Xml")
           (lib "string.ss" "Ian")
           (lib "etc.ss" "Ian")
           (lib "alphabets.ss" "Ian") ; number->string/base
           (lib "common.ss" "Ian")
           (lib "pregexp.ss")
           )
#|
read in file
"&[^[:alpha:]]" and "<[^[:alpha:]]" everywhere but in [[CDATA  (since the xml parser will do that), and wrap in one big tag
parse as xml
flatten the tree to a string: replace each entity and entity with "zzzzaz", where "a" is a base-25 number.

Convert to a tree of strings
enter regexp mania...
  (apply to the string, as well as each sub-tree)
|#

         
  (define initial-prefix false)
  ; Something short, unlikely to be found, *and* unlikely to be accidentally introduced
  ; by later regexp transformations (!)
  (define (find-non-occurring-string str)
    (let loop {[prefix-so-far initial-prefix]
               }
      (if (pregexp-match prefix-so-far str)
          (loop (string-append prefix-so-far (char->string (char+ #\a (random 26)))))
          prefix-so-far)
      ))
  (define escape-start false) ; Will be initialized later.
  (define escape-end "z")
  
  
  ;; a flaxlist ("flattened xlist") is either
  ;; - an xlist (which contains no string-xexprs), or
  ;; - a string which encodes an xlist; any non-string-xexpr
  ;;     is encoded as zzz#z, where # is a (base-25) number,
  ;;     which serves as an index into the associated table.
  ;;     That table contains flexprs.
  ;; A flexpr ("flattened xexpr") is an xexpr except that
  ;;   tag-bodies aren't xlists, they are flaxlists.

  #| A vxlist (vector of xlist)
   | Element 0 is a singleton list of the root xml tag.
   | Any string encountered is 
   |#
  (define flexpr-list false)  ;; An internal global, to help w/ xexpr->flexpr.
  (define escape-count false) ;; The length of flexpr-list
  (define (xexpr->flexpr-top x)
    (set! flexpr-list empty)
    (set! escape-count 0)
    (let* {[fx (xexpr->flexpr x)]
           [table (list->vector flexpr-list)]
           }
      (values fx table)))
            
  ;; xs: a list of xexpressions
  ;; Return list of flattened xexprs; if xs contained a string-xexpr,
  ;; then our list is a list of a single string.
  ;; 
  (define (xlist->flaxlist! xs)
    (if (find string? xs)
        (apply string-append (map xexpr->flexpr xs))
        xs))

  (define (create-flattened-escape flax)
    {begin0
      (string-append escape-start (number->string/base escape-count (drop roman-alphabet 1)) escape-end)
      (set! flexpr-list (cons flax flexpr-list))
      (add1! escape-count)
      (assert (length=? escape-count flexpr-list))
      })
  
  (define (xexpr->flexpr x)
    (cond [(tagged-xexpr? x)
           ;; (a) recursively flatten the body;
           ;; (b) make an encoded version of this tag.
           (let* {[old-bod (xexpr-body x)]
                  [new-bod (xlist->flaxlist! old-bod)]
                  [new-x (if (eq? old-bod new-bod)
                             x
                             (make-xexpr (xexpr-tag x) (xexpr-attrs x) new-bod))]
                  }
             (create-flattened-escape new-x))]
          [(entity-xexpr? x) x]
          [(string? x) x]
          [else (raise-syntax-error 'xexpr->flexpr "xexpr" x)]))


  ;; @param the-re The regular expression to replace
  ;; @param src-xexpr The xexpr to do the replacing in
  ;; @param target What to replace a match with:
  ;;    either an xexpr, or (xexpr -> xexpr)
  #;(define (re-xml-replace* the-re src-xexpr target)
      )
  )
  

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Test internal functions.
;;;; Change language to "module".
(require (lib "util.ss" "Ian")
         (lib "common.ss" "Ian")
         (lib "etc.ss" "Ian")
         (lib "string.ss" "Ian")
         )
(require/expose rekey {find-non-occurring-string
                       initial-prefix
                       create-flattened-escape
                       escape-start escape-count
                       flexpr-list
                       })

(find-non-occurring-string "hullaballoo")
(find-non-occurring-string initial-prefix)
;; "zzzazzzbzzzc...":
(define zzzs (apply string-append (map (λ1 string-append initial-prefix (char->string __)) (map (λ1 char+ #\a __) (iota 26)))))
(find-non-occurring-string zzzs)


(create-flattened-escape "#0")
(create-flattened-escape "#1")
(create-flattened-escape "#2")


#| Some old fiddling, before deciding to do xml-tag parsing first.
(pregexp-replace* "``[A-Za-z]*"
                  "hello there ``String all" 
                  (λ1 string-append "<code>" (remove-prefix __ "``") "</code>"))



(define attr+val  (string-append "\\w+\\s*=\\s*\"[^\"]*\""))
(define attr+val* (string-append "(?: " attr+val "\\s*" ")*"))
(pregexp-replace* attr+val "if align= \"xy\" and x=\"5\" and align = \"right\"" "[attr-found]")
(pregexp-replace* (string-append attr+val "+") "if align= \"xy\"  x=\"5\" align = \"right\"" "[attrs-found]")

(define tag-w/o-body (string-append "<\\w+\\s*" attr+val* "/>"))
(define tag-w-body (string-append "<(\\w+)\\s*" attr+val* ">" "(.*?)" "</\\1>"))


(pregexp-replace* tag-w/o-body "hello<br/>there" "\n")
(pregexp-replace* tag-w/o-body "hello <br />there" "\n")
(pregexp-replace* tag-w/o-body "hello <br align=\"value\" otherattr=\"otherval\" /> there" "\n")

(pregexp-replace* tag-w-body "hello <p> the<p>r</p>e</hic></p></p>" "(para:\\2)")

(pregexp-replace* (string-append "(?:" tag-w-body ")+?") "hello <p> the<p>r</p>e</hic></p></p>" "(para:\\2)")
|#
