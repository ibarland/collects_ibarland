; export: class/verbose define-class

(current-library-collection-paths
 (append (current-library-collection-paths) 
         (list "stratus:ian:src:collects-ian")))
(require-library "misc-ian.ss" "general")  ; Various helper functions.
(require-library "classd.ss")              ; Use "define"-style class syntax

(start-testing-zone "define-class.ss")



;;    (define-class some-class-name% [..xx...] ..yy..)
;; => (define some-class-name% (class/verbose [..xx..] ..yy..))
;;
;;    (define-class (some-class-name% ..init-args..) [..xx..] ..yy..)
;; => (define some-class-name% (class/verbose [(constructor-args ..init-args..) ..xx..] ..yy..))
;;    Note that ..init-args.. can be an improper list.
;;
(define (the-define-class-macro name-and-init specials . defines)
  (let*-values [{(name constructor-args)
                 (cond [(symbol? name-and-init) (values        name-and-init  false)]
                       [(pair? name-and-init)   (values (first name-and-init) (rest name-and-init))]
                       [else (raise-syntax-error 'define-class "Must start with symbol or (symbol ..init-args..)"
                                                `(define-class ,name-and-init ...))])}]
    (begin
      (if (not (char=? #\% (last-char-in-symbol name)))
          (printf "Warning: class names should end with '%' by convention, in (define-class ~s ...).~n" name))
      `(define ,name (class/verbose
                      ; Now give the specials clauses, splicing in constructor args if needed:
                      ,(cond [(false?  constructor-args) specials]
                             [else   ; constructor args is either a pair or a symbol; do same thing in both cases:
                              (cons (cons 'constructor-args constructor-args) specials)])
                      ,@defines)))))
    
(define-macro define-class the-define-class-macro)


(test (the-define-class-macro 'foo% '[(special1 is neat)] 'body1 'body2) =
      '(define foo% (class/verbose [(special1 is neat)] body1 body2)))

(test (the-define-class-macro '(foo% (init1 a1) init2) '[(special1 is neat)] 'body1 'body2) =
      '(define foo% (class/verbose [(constructor-args (init1 a1) init2) (special1 is neat)] body1 body2)))

(test (the-define-class-macro '(foo% (init1 a1) . init2) '[(special1 is neat)] 'body1 'body2) =
      '(define foo% (class/verbose [(constructor-args (init1 a1) . init2) (special1 is neat)] body1 body2)))

(test (the-define-class-macro '(foo% . init2) '[(special1 is neat)] 'body1 'body2) =
      '(define foo% (class/verbose [(constructor-args . init2) (special1 is neat)] body1 body2)))




#| 
<p>
The class/verbose macro provides a syntax for class expressions that use define-style syntax
to bind instance variables, instead of the letrec-style syntax of class.  (See also class/d.)
It further allows for the keywords "gettable" and "settable", "implements", "extends",
and it can also provides default calls to super-init and a default constructor (see below).
</p>

<p>
<quote>
<pre>
(class/verbose
   (<i>variable-spec-clause</i> ...)
   <i>expression-or-definition</i> ...)
</pre>
</quote>

<quote>
<pre>
  <i>variable-spec-clause</i> is one of:
  (extends <i>superclass-expr</i>)
  (implements <i>interface-expr</i> ...)
  (constructor-args <i>variable</i> ...)
  
  (public <i>variable</i> ...)
  (override <i>variable</i> ...)
  (inherit <i>inherit</i> ...)
  (rename (<i>variable</i> <i>variable</i>) ...)

  (gettable <i>variable-with-default</i> ...)
  (settable <i>variable</i>)
</pre>
</quote>
There can only be one <code>constructor-args</code> clause;
all the other <i>variable-spec-clause</i>s can occur repeatedly.
</p>


<p>
Extends gives the superclass; Implements is a list of interfaces being met.
A gettable variable <code>foo</code> means that <code>get-foo</code> is generated.
Slightly asymmetrically, a settable variable <code>goo</code> means that
both <code>set-foo!</code> <em>and</em> <code>get-foo</code> are
generated (since in practice it uncommon to have a field which is
settable but not gettable.)
A variable cannot be both public and gettable, nor both public and settable.
</p>

<p>
Note that if a gettable or settable variable is given an
initial value, it shouldn't be given one elsewhere.
Further, if you mention a variable only as gettable or settable
and don't provide an initial value,
it is up to you to define it in the expressions-or-definition's
(Either via define, define-values, #%define, or #%define-values.)
</p>

<p>
If no call to super-init is made, a default is provided, which just passes all the constructor-args
to the parent.  Furthermore, if no constructor-args are provided, define/verbose provides a default
constructor takes any number of arguments; since in this case the object ocde doesn't know the name
of the constructor parameter, this default constructor can only be useful if the default super-init
is also used.
Finally, note that it's undecidable whether code actually calls super-init;
we simply look for "super-init" occuring in the source.
If you do this but don't actually call super-init,
you'll have to use class/d directly, instead of class/verbose.
</p>

|#

;"About to define class/verbose"

(define (the-class/verbose-macro specials . body)
    (let* [(extends    (assoc/gather 'extends    specials '(object%)))
           (implements (assoc/gather 'implements specials empty))
           (inherit    (assoc/gather 'inherit    specials empty))
           (override   (assoc/gather 'override   specials empty))
           (public     (assoc/gather 'public     specials empty))
           (rename     (assoc/gather 'rename     specials empty))
           (gettable   (assoc/gather 'gettable   specials empty))
           (settable   (assoc/gather 'settable   specials empty))
           (constructor-args-catchAll (gensym "constructor-args-catchAll"))
           (constructor-args (assoc/gather 'constructor-args specials constructor-args-catchAll))
           (call-to-super-init-provided? (containsq? 'super-init (flatten body)))   ; Close enough, for an unsolvable problem.
          
           ; Do some error-checking before proceeding:
           ;
           (dummy!  (error-check `(class/verbose ,specials ,@body)
                                 extends implements inherit override public rename gettable settable constructor-args body
                                 call-to-super-init-provided? constructor-args-catchAll))
           
           
           ; The elements of gettables, settables are either symbols, or symbol/init-value pairs.
           ; Extract just the symbols, for creating the names of the get/set functions.
           ;
           (settable-fields (map declaration->name settable))
           (gettable-fields (append
                             settable-fields     ; ASYMMETRY: gettable includes all settable (not vice-versa).
                             (map declaration->name gettable)))
           
           (field-name->getter-name (lambda (v) (symbol-append 'get- v)))
           (field-name->setter-name (lambda (v) (symbol-append 'set- v '!)))
           
           ; The names of all the get/setters, e.g. '(set-x set-y ...).
           ; These will all be declared public.
           ;
           (getter-names (map field-name->getter-name gettable-fields))          
           (setter-names (map field-name->setter-name settable-fields))

           ; The code for the get/setters, e.g. '(define (set-x! new) (set! x new)).
           ;
           (getter-methods (map (lambda (v) `(define (,(field-name->getter-name v)) ,v) ) 
                                gettable-fields))
           (setter-methods (map (lambda (v) `(define (,(field-name->setter-name v) new) (set! ,v new)))
                                settable-fields))
           
           ;; set/gettable-init: list of initializing define's for set/gettables.
           ;; Note that if a get/settable var is initialized elsewhere,
           ;; error-check should have already reported an error.
           ;;
           (set/gettable-init
            (map (lambda (var/val-pair)
                   `(define ,(first var/val-pair) ,(second var/val-pair)))
                 (filter has-initial-value? (append gettable settable))))

           ;; Generate a call to super-init, if none was provided.
           ;; The generated default just passes the constructor-args to super-init.
           ;; BUG? What if super-init was called in one fo the init-args!?
           ;;
           (super-call (unless call-to-super-init-provided?
                           (if (list? constructor-args)
                               `(super-init ,@constructor-args)
                               ;; If the constructor-args used a catch-all argument, we need
                               ;; to de-list those caught.  Do this by using it as second arg to append:
                               `(apply super-init (append (list ,@(drop-last/dot constructor-args))
                                                          ,(last/dot constructor-args)  )))))
           ]

      ; Okay, now we throw it all back together, voila!
      ;
      `(class/d* ,(first extends)
                 ,implements
                 ,constructor-args
                 [(inherit  ,@inherit)
                  (override ,@override)
                  (public   ,@getter-names ,@setter-names ,@public)
                  (rename   ,@rename)]
                 
                 ,(unless call-to-super-init-provided? super-call)
                 ,@set/gettable-init
                 ,@getter-methods
                 ,@setter-methods
                 ,@body) ))

(define-macro class/verbose the-class/verbose-macro)







;; Helper functions for class/verbose.



;; declaration->name: decl -> symbol
;; where decl is from class/d, any of public-decl, rename-decl, etc.
;;
(define (declaration->name decl)
  (cond [(symbol? decl) decl]
        [(and (pair? decl) (symbol? (first decl)))   (first decl)]
        [(and (pair? decl) (pair?   (first decl)))   (first (first decl))]))

; e.g. consider the declarations:   (public x (y 3) ((z old-z) 4)
(test (declaration->name 'x)             = 'x)
(test (declaration->name '(y 3))         = 'y)
(test (declaration->name '((z old-z) 4)) = 'z)


;; has-initial-value?: decl -> boolean
;; Given a variable declaration from a class,
;; is it giving an initialized value to the variable?
;; Helper functions for class/verbose.
;;
;; The types of field declarations in a class can be: [from public-var-declaration]
;;   ((internal-instance-variable external-instance-variable) instance-var-initial-value-expr)
;;   (instance-variable instance-var-initial-value-expr)
;;   (instance-variable)
;;   instance-variable
;; The last two don't have initial values; the first two do.
;; All other declarations are similar looking, including inherit and rename declarations.
;; NOTE: this doesn't work for initializer-variables when using dot-notation:
;; In the arg-list (a [b 3] . c), c *will* have an initial value.
;; Of course, in constructor-args, all variables will be given values!
;;
(define (has-initial-value? decl)
  (and (pair? decl) (not (empty? (second decl)))))



                   

;"about to define error-check"


;; error-check
;; Check on the gettable/settable variables, to make sure they aren't
;; multiply initialized, that they aren't public,
;; and that settable doesn't overlap gettable (since we decided to
;; havve the semantics of settable include an accessor).
;;
;;
(define (error-check original-text
                     extends implements
                     inherit override public rename gettable settable constructor-args
                     body
                     call-to-super-init-provided? constructor-args-catchAll)
    (unless (length=1? extends) (err "Must extend exactly one class." extends))

    ; Gather info, for error-checking below:
    ;
    (let* {[err (lambda (msg) (raise-syntax-error 'class/verbose msg original-text))]
           
           [public-names    (map declaration->name public  )]   ; public variables
           [gettable-names  (map declaration->name gettable)]   ; gettable variables
           [settable-names  (map declaration->name settable)]
           [default-constructor-created? (eq? constructor-args-catchAll constructor-args)]
           }
      ;; Begin error-checking:

      ; Check for two different constructor-args specs.
      ; (Other specials [inherit, extends, etc] can be duplicated no problem.)
      (when (> (length (filter (lambda (a-special) (symbol=? 'constructor-args (first a-special))) (second original-text))) 1) 
          (err "You must provide only one constructor-args specification."))
      ; Bug: this will give an uninformative error message, if (second original-text) wasn't in right format.
      
      
      (when (and default-constructor-created? call-to-super-init-provided?)
        ; The default constructor-args just gathers arguments into a catch-all arg ...
        ; whose name is inaccessible to the user code!
        ; The only way this could be useful is if this macro also generated
        ; the default call to super-init, which passes on those args:
        (err "You must specify constructor-args (unless you forgo your call to super-init.)"))
 
      ; Make sure get/settable don't overlap with public variables.
      (check-for-public-get/set-overlap err public-names gettable-names settable-names)

      ; For each get/settable field: make sure that it is initialized.  More precisely:
      ; (a) if it IS initialized elsewhere, it must NOT be initialized in get/settable;
      ; (b) if it isn't initialized in get/settable, it MUST be initialized elsewhere
      ;     (possibly down in the body/expr-list).
      ;
      (check-for-get/settable-initialized err body
                                          public inherit rename constructor-args gettable settable
                                          gettable-names settable-names)
    ))



;"more helpers for error-check"



;; Determine whether any variables occur in both get/setabble clauses, and in public clauses.
;; helper for error-check.
;;
(define (check-for-public-get/set-overlap err public-names gettable-names settable-names )
  (let* {[public-gettable (intersectq   public-names gettable-names)]  ; Any public   AND gettable?
         [public-settable (intersectq   public-names settable-names)]
         [get/settable    (intersectq gettable-names settable-names)]  ; Any gettable AND settable?
         }
    (cond 
      [(not (empty? get/settable))
       (err (format "Settable variables are automatically gettable as well; declare ~s as settable (only)." get/settable))]
    
      [(not (empty? public-gettable))
       (err (format "~s can't be both public and gettable." public-gettable))]
      [(not (empty? public-settable))
       (err (format "~s can't be both public and settable." public-settable))]
      
      [else 'huzzah!])))



;"about to try check-get/settable-initialized"

; check-for-get/settable-initialized
; For each get/settable field: make sure that it is initialized.  More precisely:
; (a) if it IS initialized elsewhere, it must NOT be initialized in get/settable;
; (b) if it isn't initialized in get/settable, it MUST be initialized elsewhere
;     (possibly down in the body/expr-list).
;
; Note that init-elsewhere is the list of variables initialized in
; inherit, rename, public, and constructor-args (but NOT down in the body/expr-list).
;
(define (check-for-get/settable-initialized err body 
                                            public inherit rename constructor-args gettable settable
                                            gettable-names settable-names)
  (letd* {[init-in-get/settable      ; All variables which receive initial values in get/settable:
             (map declaration->name (filter has-initial-value? (append gettable settable)))]
            
            [init-elsewhere     ; All variables which receive initial values somewhere besides get/settable:
             (append (map declaration->name (filter has-initial-value? (append public inherit rename)))
                     ; All constructor-args are considered initialized:
                     (cond [(or (pair? constructor-args) (empty? constructor-args))
                            (improper->proper (map/dot declaration->name constructor-args))]
                           [(symbol? constructor-args)
                            ; This case needed if original was "(class/verbose [(constructor-args . x) ...])",
                            ; then here constructor-args is 'x, not even a list.  Allow for this:
                            (list constructor-args)]))]
            [(check-for-multiple-inits v)
             (cond [(and (containsq? v init-in-get/settable) (containsq? v init-elsewhere))
                    (err (format "Get/settable field ~s is initialized elsewhere; ~a"
                                 v "it may NOT be re-initialized in get/settable."))]
                   [(and (containsq? v init-in-get/settable) (not (containsq? v init-elsewhere)))
                    (when (defined-in-expr-list? v body)
                      (err (format "Field ~s initialized in get/settable ~a"
                                   v "should not be defined down in exprs-and-defs.")))]
                   [(and (not (containsq? v init-in-get/settable)) (containsq? v init-elsewhere))
                    'everything-okay:-v-inited-but-not-in-get/set-clause]
                   [(and (not (containsq? v init-in-get/settable)) (not (containsq? v init-elsewhere)))
                    (unless (defined-in-expr-list? v body)
                      (err (format "Get/settable field ~s must be initialized or defined in body." v)))])
             ]}
    
    (for-each check-for-multiple-inits (append gettable-names settable-names))))


;;; Yet more helper functions for error-check.
;"about to define yet more helpers"

;; gather-exprs-starting-with-open: (list-of sexpr), (list-of symbol) --> (list-of sexpr)
;; Given exprs, return those of the form "(<baz> ...)", for any <baz> in start-words.
;;
(define (gather-exprs-starting-with-open exprs start-words)
  (filter (lambda (expr) (and (pair? expr) (containsq? (first expr) start-words))) exprs))


;; define-in-expr-list?: symbol (list-of sexpr) --> boolean
;; In the body/expr-list, is any of them "(define v ...)" or "(define-values (...v...) ...)"?
;; (Or, #%define, #%define-values.)
;;
(define (defined-in-expr-list? v exprs)
  (let* [(defines     (gather-exprs-starting-with-open exprs (list 'define '#%define)))
         (define-vals (gather-exprs-starting-with-open exprs (list 'define-values '#%define-values)))]
    (or (ormap (lambda (def) (or (eq? (second def) v)                                      ; "(define v ...)"
                                 (and (cons? (second def)) (eq? v (first (second def)))))) ; "(define (f x) ..."
               defines)
        (ormap (lambda (def-val) (containsq? v (second def-val)))
               define-vals))))


; Test of define-in-expr-list?:
;
(define sample-def-list
  '((define x 3)
    (define y 7)
    (#%define z 9)
    (define (f x) (+ x 29))
    (some-other-statement n)
    (#%define (g xx) 'w)
    (define-values (a b c) 'x 'y 'z)
    (#%define-values (j k l) 1 2 3)
    (+ p 37)))

(test (defined-in-expr-list? 'x  sample-def-list) = true)
(test (defined-in-expr-list? 'n  sample-def-list) = false)
(test (defined-in-expr-list? 'xx sample-def-list) = false)
(test (defined-in-expr-list? 'a  sample-def-list) = true)
(test (defined-in-expr-list? 'k  sample-def-list) = true)



;;------------
;; Some tests of the whole thing.
(test
(apply the-define-class-macro
       '(foo%
         [ (public x)
           (constructor-args) ; 0-arg constructor.
           (gettable (g 3))
           (settable (s 5))]
         (super-init)
         (define x 5)))
=
'(define foo%
   (class/verbose
    [(public x)
     (constructor-args)
     (gettable (g 3))
     (settable (s 5))]
    (super-init)
    (define x 5)))
)


; Now do it for real, and get some objects of that class:
(define-class foo%
              [ (public x)
                (constructor-args) ; 0-arg constructor.
                (gettable (g 3))
                (settable (s 5))]
              (super-init)
              (define x 5))
       
(define f (make-object foo%))
(test (ivar f x) = 5)
(test (send f get-s) = 5)
(test (send f set-s! 19) = (void))
(test (send f get-s) = 19)
; (send f set-g! 'uh-oh)   ERROR: set-g! not found in foo%

;; warning: class-name not ending in %
;(define-class unpercent [ ] (super-init))
;(make-object unpercent)

; ERROR: privvy must be initialized:
(define-class foo%
             [ (public x)
               (constructor-args j (a 3) (b 7) (g 12))
               (gettable g (i 7) j privvy )
               (settable (s 5) (t 7))]
             (super-init)
             (define x 5)
             (define (privvy z) 9)
             ;(define privvy 5)
             ;(define-values (tt u privvy) (values 4 5 6))
             ;(#%define (privvy z) 9)
             (define top-secret 'flubber))

(define a-foo (make-object foo% 19 20))
(test (send a-foo get-j) = 19)
;(test (send a-foo get-privvy) = (+ 20 14))
; (ivar a-foo a) =  ;error: a not found [private]

; (send a-foo set-privvy! 29)    ;error: set-privvy! not found

(define bovine% (class/verbose [(constructor-args a (b 3) . c) (public moo)]
                              (define (moo) c) (super-init) ))
(define bessie (make-object bovine% 3 4 'i' feel 'so 'low))
(test (send bessie moo) = '(i feel so low))

;(class/verbose [(public (x 7))] )  ; error: (x 7) not allowed in class/d's public!


(define-class fred%
              [(constructor-args x y z . et-al)
               (gettable et-al)
               ])


(test (send (make-object fred% 8 9 10 11 12) get-et-al) = '(11 12))

; Test the making of default constructors, which just pass their args to super-init:
;
(define-class fredson%
              [(extends fred%)])

(test (send (make-object fredson% 3 4 5 6 7 8) get-et-al) = '(6 7 8))

; Error: two constructors:
