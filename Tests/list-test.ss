#lang scheme
(require rackunit)
(require (lib "etc.ss"))
(require (lib "list.ss"))
(require (lib "list.ss" "Ian"))
(require (lib "util.ss" "Ian"))
(require (lib "number.ss" "Ian"))  ; "divides?", for tests.

(test (atom? 3)      = true)
(test (atom? empty)  = true)
(test (atom? '(3 4)) = false)
;(test (atom? (make-posn 3 4)) = true) ; Counterintuitive.

(test (non-empty? 3)      = false)
(test (non-empty? empty)  = false)
(test (non-empty? '(3 4)) = true)

"length functions"
(test*/unary length=1?  = `{[,empty ,false] [(1) ,true ] [(1 2 3) ,false]})
(test*/unary length<=1? = `{[,empty ,true ] [(1) ,true ] [(1 2 3) ,false]})
(test*/unary length>=1? = `{[,empty ,false] [(1) ,true ] [(1 2 3) ,true ]})
(test*/unary length>1?  = `{[,empty ,false] [(1) ,false] [(1 2 3) ,true ]})
(test*/unary length<1?  = `{[,empty ,true ] [(1) ,false] [(1 2 3) ,false]})

(test*/unary length<=2? = `{[,empty ,true ] [(a) ,true ] [(x y) ,true ] [(1 2 3) ,false]})
(test*/unary length<2?  = `{[,empty ,true ] [(a) ,true ] [(x y) ,false] [(1 2 3) ,false]})
(test*/unary length>=2? = `{[,empty ,false] [(a) ,false] [(x y) ,true ] [(1 2 3) ,true ]})
(test*/unary length>2?  = `{[,empty ,false] [(a) ,false] [(x y) ,false] [(1 2 3) ,true ]})
(test*/unary length=2?  = `{[,empty ,false] [(a) ,false] [(x y) ,true ] [(1 2 3) ,false]})

(test*/unary (lambda (x) (length<=? 2 x)) = `{[,empty ,true ] [(a) ,true ] [(x y) ,true ] [(1 2 3) ,false]})
(test*/unary (lambda (x) (length<?  2 x)) = `{[,empty ,true ] [(a) ,true ] [(x y) ,false] [(1 2 3) ,false]})
(test*/unary (lambda (x) (length>=? 2 x)) = `{[,empty ,false] [(a) ,false] [(x y) ,true ] [(1 2 3) ,true ]})
(test*/unary (lambda (x) (length>?  2 x)) = `{[,empty ,false] [(a) ,false] [(x y) ,false] [(1 2 3) ,true ]})
(test*/unary (lambda (x) (length=?  2 x)) = `{[,empty ,false] [(a) ,false] [(x y) ,true ] [(1 2 3) ,false]})

(define ten-letters '(a b c d e f g h i j))
(define ten-getters (list 1st 2nd 3rd 4th 5th 6th 7th 8th 9th 10th))
(check-equal? (map (λ(gettr) (gettr ten-letters)) ten-getters)
              ten-letters)


"list-ref, take, drop (w/ safe variants)" 
(test (list-ref1 '(a b c d) 1) = 'a)
(test (list-ref1 '(a b c d) 2) = 'b)
(test (list-ref1 '(a b c d) 4) = 'd)

(check-eq? (list-ref/safe '(a b c d)  0) 'a)
(check-eq? (list-ref/safe '(a b c d)  2) 'c)
(check-eq? (list-ref/safe '(a b c d)  4) #f)
(check-eq? (list-ref/safe '(a b c d) -1) #f)
(check-eq? (list-ref/safe '(a b c d)  0 'blah) 'a)
(check-eq? (list-ref/safe '(a b c d)  2 'blah) 'c)
(check-eq? (list-ref/safe '(a b c d)  4 'blah) 'blah)
(check-eq? (list-ref/safe '(a b c d) -1 'blah) 'blah)
(check-eq? (list-ref/safe empty 0 'blah) 'blah)
(check-eq? (list-ref/safe empty 0) #f)


(test (drop-last '(a)) = '())
(test (drop-last '(a b c)) = '(a b))
(test (last '(a)) = 'a)
(test (last '(a b c)) = 'c)
(test (drop-right-while even? '()) = '())
(test (drop-right-while even? '(2 4 6)) = '())
(test (drop-right-while even? '(2 3 4 5)) = '(2 3 4 5))
(test (drop-right-while even? '(2 3 4 5 6)) = '(2 3 4 5))
(test (drop-right-while even? '(3 2 4 4)) = '(3))

(test (take/safe '(a b c d) 0) = '())
(test (take/safe '(a b c d) 1) = '(a))
(test (take/safe '(a b c d) 2) = '(a b))
(test (take/safe '(a) 0) = '())
(test (take/safe '(a) 1) = '(a))
(test (take/safe '(a) 2) = '(a))
(test (take/safe '() 0) = '())
(test (take/safe '() 1) = '())
(test (take/safe '() 2) = '())
(test (drop/safe '(a b c d) 0) = '(a b c d))
(test (drop/safe '(a b c d) 1) = '(b c d))
(test (drop/safe '(a b c d) 2) = '(c d))
(test (drop/safe '(a) 0) = '(a))
(test (drop/safe '(a) 1) = '())
(test (drop/safe '(a) 2) = '())
(test (drop/safe '() 0) = '())
(test (drop/safe '() 1) = '())
(test (drop/safe '() 2) = '())

(test (take/safe '(a b c d) 0 'hiya) = '())
(test (take/safe '(a b c d) 1 'hiya) = '(a))
(test (take/safe '(a b c d) 2 'hiya) = '(a b))
(test (take/safe '(a) 0 'hiya) = '())
(test (take/safe '(a) 1 'hiya) = '(a))
(test (take/safe '(a) 2 'hiya) = '(a hiya))
(test (take/safe '() 0 'hiya) = '())
(test (take/safe '() 1 'hiya) = '(hiya))
(test (take/safe '() 2 'hiya) = '(hiya hiya))



(test (take<= '(a b c d) 0) = '())
(test (take<= '(a b c d) 1) = '(a))
(test (take<= '(a b c d) 2) = '(a b))
(test (take<= '(a) 0) = '())
(test (take<= '(a) 1) = '(a))
(test (take<= '(a) 2) = '(a))
(test (take<= '() 0) = '())
(test (take<= '() 1) = '())
(test (take<= '() 2) = '())
(test (drop<= '(a b c d) 0) = '(a b c d))
(test (drop<= '(a b c d) 1) = '(b c d))
(test (drop<= '(a b c d) 2) = '(c d))
(test (drop<= '(a) 0) = '(a))
(test (drop<= '(a) 1) = '())
(test (drop<= '(a) 2) = '())
(test (drop<= '() 0) = '())
(test (drop<= '() 1) = '())
(test (drop<= '() 2) = '())

(test (take<= '(a b c d) 0 'hiya) = '())
(test (take<= '(a b c d) 1 'hiya) = '(a))
(test (take<= '(a b c d) 2 'hiya) = '(a b))
(test (take<= '(a) 0 'hiya) = '())
(test (take<= '(a) 1 'hiya) = '(a))
(test (take<= '(a) 2 'hiya) = '(a hiya))
(test (take<= '() 0 'hiya) = '())
(test (take<= '() 1 'hiya) = '(hiya))
(test (take<= '() 2 'hiya) = '(hiya hiya))


(test (take-right/safe '(a b c d) 0) = '())
(test (take-right/safe '(a b c d) 1) = '(d))
(test (take-right/safe '(a b c d) 2) = '(c d))
(test (take-right/safe '(a) 0) = '())
(test (take-right/safe '(a) 1) = '(a))
(test (take-right/safe '(a) 2) = '(a))
(test (take-right/safe '() 0) = '())
(test (take-right/safe '() 1) = '())
(test (take-right/safe '() 2) = '())
(test (drop-right/safe '(a b c d) 0) = '(a b c d))
(test (drop-right/safe '(a b c d) 1) = '(a b c))
(test (drop-right/safe '(a b c d) 2) = '(a b))
(test (drop-right/safe '(a) 0) = '(a))
(test (drop-right/safe '(a) 1) = '())
(test (drop-right/safe '(a) 2) = '())
(test (drop-right/safe '() 0) = '())
(test (drop-right/safe '() 1) = '())
(test (drop-right/safe '() 2) = '())

(test (take-right/safe '(a b c d) 0 'hiya) = '())
(test (take-right/safe '(a b c d) 1 'hiya) = '(d))
(test (take-right/safe '(a b c d) 2 'hiya) = '(c d))
(test (take-right/safe '(a) 0 'hiya) = '())
(test (take-right/safe '(a) 1 'hiya) = '(a))
(test (take-right/safe '(a) 2 'hiya) = '(hiya a))
(test (take-right/safe '() 0 'hiya) = '())
(test (take-right/safe '() 1 'hiya) = '(hiya))
(test (take-right/safe '() 2 'hiya) = '(hiya hiya))








(test (snoc 'd '(a b c)) = '(a b c d))
(test (snoc 'a empty)    = '(a))
;(test (snoc* 'd 'e '(a b c)) = '(a b c d e))
;(test (snoc! 'd '(a b c)) = '(a b c d))
;(test (snoc! 'a empty)    = '(a))

(check-equal? (cons-if #t 'a '()) '(a))
(check-equal? (cons-if #f 'a '()) '())
(check-equal? (cons-if #t 'a '(b c)) '(a b c))
(check-equal? (cons-if #f 'a '(b c)) '(b c))

(test (flatten '(3 4 5))    = '(3 4 5))
(test (flatten '(3 (4 5)))  = '(3 4 5))
(test (flatten '())         = '())
(test (flatten '(() () ())) = '())
(test (flatten '(((((3 ())) 4)) ((5)) 6 ())) = '(3 4 5 6))



(test*/unary (lambda (lst) (flatten lst 1)) equal?
       '{[(3 4 5) (3 4 5)]
         [(3 (4) 5) (3 4 5)]
         [(3 ((4)) 5) (3 (4) 5)]
         [((((3)))) (((3)))]
         [() ()]})

"invert-alist (should start with one warning)"
(check-equal? (invert-alist '((a   4) (b   5)))  '(((4) . a) ((5) . b)))  ; !!! give warning
(check-exn exn:fail? (thunk (invert-alist '((a . 4) (b   5)) #t)))

(check-equal? (invert-alist empty) empty)
(check-equal? (invert-alist '((a . 4) (b . 5)))  '((4 . a) (5 . b)))


(check-equal? (invert-alist '((a   4) (b   5)) #f)  '(((4) . a) ((5) . b)))
(check-equal? (invert-alist '((a   4) (b   5)) #t)  '(( 4  a) ( 5  b)))  




             
(test (intersperse 'har '()) = empty)
(test (intersperse "! " '("i")) = '("i"))
(test (apply string-append (intersperse "! " '("i" "am" "very" "serious" "all" "the"
 "time?")))
      =
      "i! am! very! serious! all! the! time?")


(test (filter-out   even? '(3 2 6 5)) = '(3 5))
(test (filter-out   even? '(3 1 5 7)) = '(3 1 5 7))
;; Now make sure the binary form works:
(test (filter/c     > 4 '(3 5 2 8))   = '(5 8))
(test (filter-out/c > 4 '(3 5 2 8))   = '(3 2))
(test (filter/c     divides? 8 '(4 3 16 8)) = '(4 8))
(test (filter-out/c divides? 8 '(4 3 16 8)) = '(3 16))

(test (map-voidless display '(3 4 5)) = empty)
(test (map-voidless (lambda (x) (when (number? x) (+ x 1))) '(3 4 apple 5)) = '(4 5 6))

(test (map/append list '(1 2 3 4)) = '(1 2 3 4))
(test (map/append cons? empty)     = empty)
(test (map/append cons '(1 2 3) `((apple orange) (tutu) ,empty)) = '(1 apple orange 2 tutu 3))

(test (intersplice '(hey ho) '(one two three)) = '(one hey ho two hey ho three))

"assocs"
(test (assoc/default 'hi  '{[hi ho] [bye aloha]} "not found") = 'ho)
(test (assoc/default 'bye '{[hi ho] [bye aloha]} "not found") = 'aloha)
(test (assoc/default 'buy '{[hi ho] [bye aloha]} "not found") = "not found")
(test (assoc/gather  'hi  '{[hi ho] [bye aloha]} "not found") = `(ho))
(test (assoc/gather  'bye '{[hi ho] [bye aloha]} "not found") = `(aloha))
(test (assoc/gather  'buy '{[hi ho] [bye aloha]} "not found") = "not found")
(test (assoc/gather  'buy '{[hi ho] [bye aloha]}            ) = empty)
(test (assoc/gather  'hi  '{[hi ho] [bye aloha] [hi hum]}   ) = `(ho hum))
       
       
(test (assoc1  'slim '{[hi there] [slim pickins] [up down] [hi aloha]}) = 'pickins)
(test (assoc01 'slim '{[hi there] [slim pickins] [up down] [hi aloha]}) = 'pickins)
(test (assoc01 'sly  '{[hi there] [slim pickins] [up down] [hi aloha]}) = false)
(test (assoc01 'slim '{[hi there] [slim pickins] [up down] [hi aloha]} 'the-fault) = 'pickins)
(test (assoc01 'sly  '{[hi there] [slim pickins] [up down] [hi aloha]} 'the-fault) = 'the-fault)

"filters"
(test (filter1  even? '(1 3 5 7 8 9 11)) = 8)
(test (filter01 even? '(1 3 5 7 8 9 11)) = 8)
(test (filter01 even? '(1 3 5 7   9 11)) = false)
(test (filter01 even? '(1 3 5 7 8 9 11) 'dah-fault) = 8)
(test (filter01 even? '(1 3 5 7   9 11) 'dah-fault) = 'dah-fault)
 
#|
"some errors:"
(test (filter01 even? '(1 3 5 7 8 9 10)) = 'error)
(test (filter1  even? '(1 3 5 7   9 11)) = 'error)
(test (filter1  even? '(1 3 5 7 8 9 10)) = 'error)
|#

#| ; See srfi-1's delete-duplicates.
   ; For our 3rd argument "better", see (lib "roman-numeral.ss" "Ian")
(test (remove-duplicates '(3 4 5 6 7 4 2 3 9)) 
                       = '(3 4 5 6 7 2 9))
(test (remove-duplicates '{[3 hi] [7 aloha] [2 hi] [11 howdy] [9 hi] [3 howdy] [2 bye]}
                         (lambda (a b) (symbol=? (second a) (second b))))
      = '{[3 hi] [7 aloha] [11 howdy] [2 bye]})
(test (remove-duplicates '{[3 hi] [7 aloha] [2 hi] [11 howdy] [9 hi] [3 howdy] [2 bye]}
                         (lambda (a b) (symbol=? (second a) (second b)))
                         (lambda (a b) (> (first a) (first b))))
      = '{[9 hi] [7 aloha] [11 howdy] [2 bye]})
|#


(require scheme/mpair)
(test (last-mpair (mlist 3 4 5)) = (mlist 5))
(test (last-mpair (mlist 3)) = (mlist 3))
(test (last-mpair (mcons 3 4)) = (mcons 3 4))

(define x (mlist 4 5 6 7))
(define x-circ (shared {[z (mcons 4 (mcons 5 (mcons 6 (mcons 7 z))))]} z))
"Three equal shared lists:"  ; NB Calling equal? will recur.
x-circ
(circularize! x)
x

#|
(test (for 3 <  6) = '(3 4 5))
(test (for 3 <= 6) = '(3 4 5 6))
(test (for 3 <  6  2) = '(3 5))
(test (for 6 >  3 -2) = '(6 4))
(test (for 3 < 2)    = '())
(test (for 3 < 2 -1) = '())
|#

(test (until empty? '(a b c) rest) = '{(a b c) (b c) (c)})
(test (until empty? '()      rest) = '{})
(test (while cons?  '(a b c) rest) = '{(a b c) (b c) (c)})
(test (while cons?  '()      rest) = '{})





;;; todo: wrap these errors in catch:
;"About to fail twice, the second time fatally:"
;(test (assoc1  'hi   '{[hi there] [slim pickins] [up down] [hi aloha]}) = 'there)
;(test (assoc1  'sly  '{[hi there] [slim pickins] [up down] [hi aloha]}) = 'there)

"span="

(test (span= = '() '()) = '(() () ()))
(test (span= = '(3) '(3)) = '((3) () ()))
(test (span= = '(3 4) '(3 4)) = '((3 4) () ()))
(test (span= = '(3 4) '(3 5)) = '((3) (4) (5)))
(test (span= = '(3 4 6) '(3 4 9 9)) = '((3 4) (6) (9 9)))
(test (span= = '(3 4 6) '(9 9 9 9)) = '(() (3 4 6) (9 9 9 9)))
(test (span= = '() '(1 2 3)) = '(() () (1 2 3)))
(test (span= = '(1 2 3) '()) = '(() (1 2 3) ()))


"list-has-prefix?"
(test (list-has-prefix? '()    '()) = true)
(test (list-has-prefix? '()    '(a)) = true)
(test (list-has-prefix? '(a)   '(a)) = true)
(test (list-has-prefix? '()    '(a b c 9 d)) = true)
(test (list-has-prefix? '(a)   '(a b c 9 d)) = true)
(test (list-has-prefix? '(a b) '(a b c 9 d)) = true)
(test (list-has-prefix? '(a b c 9 d) '(a b c 9 d)) = true)
(test (list-has-prefix? '(a) '()) = false)
(test (list-has-prefix? '(a b c) '()) = false)
(test (list-has-prefix? '(a b c) '(a b)) = false)
(test (list-has-prefix? '(a b c) '(b c)) = false)

(test (list-has-prefix? '()    '(a b c d) '(a b e) '(a b) '(a b x x x)) = true)
(test (list-has-prefix? '()    '(a b c d) '(a b e) '()    '(a b x x x)) = true)
(test (list-has-prefix? '(a b) '(a b c d) '(a b e) '(a b) '(a b x x x)) = true)
(test (list-has-prefix? '(a b) '(a b c d) '(a b e) '(a)   '(a b x x x)) = false)
(test (list-has-prefix? '(a z) '(a b c d) '(a b e) '(a b) '(a b x x x)) = false)


"list-has-suffix?"
(test (list-has-suffix? '()    '()) = true)
(test (list-has-suffix? '()    '(a)) = true)
(test (list-has-suffix? '(a)   '(a)) = true)
(test (list-has-suffix? '()    '(a b c 9 d)) = true)
(test (list-has-suffix? '(d)   '(a b c 9 d)) = true)
(test (list-has-suffix? '(9 d) '(a b c 9 d)) = true)
(test (list-has-suffix? '(a b c 9 d) '(a b c 9 d)) = true)
(test (list-has-suffix? '(a) '()) = false)
(test (list-has-suffix? '(a b c) '()) = false)
(test (list-has-suffix? '(c a b) '(a b)) = false)
(test (list-has-suffix? '(b c a) '(b c)) = false)

(test (list-has-suffix? '()    '(a b c d) '(a b e) '(a b) '(a b x x x)) = true)
(test (list-has-suffix? '()    '(a b c d) '(a b e) '()    '(a b x x x)) = true)
(test (list-has-suffix? '(a b) '(c d a b) '(e a b) '(a b) '(x x x a b)) = true)
(test (list-has-suffix? '(a b) '(c d a b) '(e a b) '(b)   '(x x x a b)) = false)
(test (list-has-suffix? '(z b) '(c d a b) '(e a b) '(a b) '(x x x a b)) = false)


(check-equal? (duplicates '()) '())
(check-equal? (duplicates '(a b c d)) '())
(check-equal? (duplicates '(a b c d e d x b)) '(d b))
