#lang scheme
(require (lib "file.ss"))

;(require (lib "string.ss" "srfi" "13"))
(require (lib "string.ss" "Ian"))

;; todo: for load, capture stdout?
;;
(for-each (lambda (f) (begin (printf "~nAbout to load ~v.~n" f)
                             (require (file f))))
          (filter (lambda (fn) (has-suffix? (path->string fn) ".ss")) (directory-list)))
