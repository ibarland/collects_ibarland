#|  library functions to write?:

intersperse --> intersperse
filter-in&out  -->  srfi's "partition" which is same but returns two values
debug-outvar [varname],  or  debug-out: if given a var, then format it.





; xml.ss: "process" doesn't really eval l-to-r,
; as witnessed by using several "scheme" tags.
;   When done: re-arrange order of setEntryOPtions, in survey.html
;
; don't subprocess xexpr-tags as entities.
;
 ; make "splice" an optional arg to register
 ;
 ; recursively call "process" on the results of
 ; process, if any changes.
;
; allow xml tags <'>...</'>, <`>...<\`>, and <;>...</;>  ???

;;;; fix: in or-intro,... fix spurious emphasis.




; function.ss:
  write curry, or some sort of "curryXXX" which takes the >last< arg?:
  cf: (filter/c > 4 ...)    vs.   (filter (curryXXX > 4) ...) 


;number.ss:
  Make sure test cases check imaginary, nan, inf, and combinations thereof.
  Should practically=? be named "fairly=?",
  and a different function be written
  to 'detect' round-off error?
  E.g. in "log-base", detecting round-off error
   between result and (round result):
   can't use practically=?, if arg were 5*(10^100000),
   then it always gets rounded to exactly 100000 no matter what.
  Maybe the question is, do we want both a relative and absolute
  form of practically=?'s tolerance?
 
  
  
; move "atom?" from etc.ss to list.ss?
; Separate test into its own module?  (assert, warn?)
 
; sets:
   ;;***
   make-range ?  (just "range"?) **  variant "range1"
   range->list
   (make-)ranges  ?
   This is a verion of "iterator"?
   
; util:
   make-parameter [init];
   (parameter-push! debug true), ... (parameter-pop! debug)
   Grab "test" macro from stratus.
   define*, or define-overload, or overload, or case-define (like case-lambda)
   test-map, "tests"
   write assert-type
   put function's "non" here, so that "common.ss" includes it?
   

; trace: debug-prints args before and after call.
; instrument, which times the length of the call?
; tracify! [macro], which changes the function to that version?
   
   
   ; Drscheme option "rename", which changes all occurrences.
   ; This is just find/replace-all; double-click shortcut?


|#

(require-library "match.ss")

(match '(3 (4) 5) 
  [(a) '(unary a)]
  [(x y) `(binary ,y ,x)]
  [(a b c) (list 'ternary c b a)]
  )
