(define (symbol-append . syms)
  (string->symbol (foldr string-append "" (map symbol->string syms))))
(symbol-append 'hi 'there 'all) = 'hithereall
(symbol-append 'bye) = 'bye
(symbol-append) = '||






(define-syntax (define-setter/getter var-val)
  (syntax-case var-val (setter getter)
    [(_ name init-val)
     (let* {[setter (symbol-append 'set- (syntax-e name) '!)]}
       (syntax (define setter (lambda (val) (set! foo val)))))]))

(define stayt 3)
(define-setter/getter stayt 7)