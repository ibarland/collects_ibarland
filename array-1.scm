;;; array.ss
;;; Two-dimensional arrays, and helper functions.
;;;
;;;  Todo: write array*, which is any # of dimensions?
;;;
(module array mzscheme
  (provide

   build-array
   make-array
   array-ref
   array?
   array-set!
   array-inc!
   array-rows
   array-columns
   array-size
;   array-init!
;   array-copy!
;   array-map!
;   array-print
   )

  (require (lib "common.ss" "Ian"))


(define-struct array-certificate (num-dims num-rows num-cols all-dims))

;; array-certificate: array -> array-certificate
;; For internal use only -- may crash if passed a non-array.
;;
;; (Hopefully this name overloading is okay.)
;;
;(define (array-certificate arr) 
;  )
  
  (define (valid-dimension-size? n)
    (assert-type (and/f integer? (non negative?)) n
                 'valid-dimension-size? "non-negative integer")
    true)
  
;; build-array: num num (num num --> alpha) --> (array-of alpha)
;; init-func takes the row,column numbers and returns the initial value
;; for the given cell.
;;
  
(define (build-array rows cols init-func)
  (assert (valid-dimension-size? rows))
  (assert (valid-dimension-size? cols))
  (build-vector (1+ rows) ; one extra index, to hold certificate.
                (lambda (r) 
                  (if (< r rows)
                      (build-vector cols (lambda (c) (init-func r c)))
                      (make-array-certificate 2 rows cols (list rows cols))))))

;; make-array
;; Like build-array with init-func is constant function returning init-val.
;; If the init-val is omitted, then 0 is used.
;;
(define/opt (make-array rows cols  [init-val 0])
    (build-array rows cols (lambda (r c) init-val)))




;; array-ref: (array-of alpha) num num --> alpha
;;
(define (array-ref arr r c)
  (assert     (array-ok arr r c))
  (vector-ref (vector-ref arr r) c))

;; possible-certificate: ANY --> ANY
;; if given an array, return its certificate,
;; else return something else.
;; For internal use only.
;;
(define (possible-certificate arr)
  (if (and (vector? arr) (positive? (vector-length arr)))
      (vector-ref arr (1- (vector-length arr)))
      false))


;; array?: ANY --> boolean
;; Is the object an array?
;;
;;
(define (array? arr) (array-certificate? (possible-certificate arr)))

;; array-ok: array -> array-certificate
;; Verify arr is an array, and that the requested indices check out.
;; Return the array-certificate (just for fun).
;;
(define array-certificate/verify
  (case-lambda
   [(arr)
    (let* {[cert (possible-certificate arr)]}
      (assert (array-certificate? cert))
      cert)]
   [(arr r c)
    (let* {[certif  (array-certificate/verify   arr)]
           [numrows (array-certificate-num-rows certif)]
           [numcols (array-certificate-num-cols certif)]
           }
      (assert (<= 0 r))
      (assert (<= 0 c))
      (assert (<  r numrows))
      (assert (<  c numcols))
      certif)]))

(define array-ok array-certificate/verify)
         
;; array-set!: (array-of alpha) num num alpha --> (void)
;;
(define (array-set! arr r c val)
  (array-ok arr r c)
  (vector-set! (vector-ref arr r) c val))

;; array-inc!: (array-of number) num num num --> (void)
;; Increment the indicated cell (r,c) by inc.
;;
(define/opt (array-inc! arr r c [inc 1])
  (array-ok arr r c)
  (array-set! arr r c (+ inc (array-ref arr r c))))

;; array-rows:    array --> number
;; array-columns: array --> number
;; array-size:    array --> (list number number)
;;
(define (array-rows arr)  
  (array-certificate-num-rows    (array-certificate/verify arr)))
(define (array-columns arr)
  (array-certificate-num-cols    (array-certificate/verify arr)))
(define (array-size arr)
  (array-certificate-all-dims (array-certificate/verify arr)))





       #|
;; array-copy!: array array num num --> (void)
;; copy arr-src into arr-dest, offest by dr,dc.
;; Leave unchanged the parts of arr-dest not touched.
;;
(define (array-copy! arr-dest arr-src dr dc)
  ; Only part of arr-dest will be modified:
  ; Find the top,bottom,left,right of that region of arr-dest.
  (let* [(top    (clamp dr                             0 (array-rows    arr-dest)))
         (bottom (clamp (+ dr (array-rows    arr-src)) 0 (array-rows    arr-dest)))
         (left   (clamp dc                             0 (array-columns arr-dest)))
         (right  (clamp (+ dc (array-columns arr-src)) 0 (array-columns arr-dest)))]
    (do-range2 top bottom left right
               (lambda (r c _) (array-set! arr-dest r c (array-ref arr-src (- r dr) (- c dc)))) 
               (void))))


;; array-init!: array, alpha --> array
;; Fill arr with val (destroying old contents).
;;
(define (array-init! arr val)
  (array-map! arr (lambda (old r c) val)))


;; do-array: (array-of alpha) (alpha num num beta --> beta) beta --> beta
;; Call (fn r c answer-so-far) on each element (r,c) of arr.
;; Row-major order.
;;
(define (do-array arr fn answer-so-far)
  (do-range2 0 (array-rows arr) 0 (array-columns arr)  
             (lambda (r c so-far) (fn (array-ref arr r c) r c so-far))
             answer-so-far))

;; array-map: (array-of alpha), (alpha -> beta) --> (array-of beta)
;; Return a new array the same size as r, but with fn applied to each element.
;;
(define (array-map fn arr)
  (build-array (array-rows arr) (array-columns arr)
               (lambda (i j) (fn (array-ref arr i j)))))

;; map-array!: (array-of alpha), (alpha -> beta) --> (void)
;; Mutate each element of arr to be (fn! val r c)
;;
(define (array-map! arr fn!)
  (do-array arr (lambda (val r c acc) (array-set! arr r c (fn! val r c))) (void)))

;; array-print: array --> (void)
;; Print out the array.
;;
(define (array-print arr)
  (do-array arr
            (lambda (val r c _)
              (printf "~s" val)
              (printf (if (>= c (sub1 (array-columns arr))) "~n" "   ")))
            (void)))

   
(if debug? (begin (printf "~n") (array-print test-arr)))


;; array-sum: (array-of num) --> num
;; Sum all the elements of an array.
;;
(define (array-sum arr)
  (do-array arr 
             (lambda (val r c sum-so-far) (+ val sum-so-far)) 0))

(test (array-sum test-arr) = 1343)

|#
  )
