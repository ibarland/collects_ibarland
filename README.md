# Collects_ibarland

My personal racket libraries.
Many utility-functions (some of which have been superceded by more recent racket standard-libs).
The Xml/ sub-directory has a bunch of tags I've defined (and which need to be migrated to `pollen`).