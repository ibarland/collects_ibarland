(module boolean mzscheme #;(lib "plt-pretty-big.ss" "lang")  ; 'mzscheme' doesn't have 'first', 'foldl'?
        (require (lib "list.ss"))
  (provide
   false?  true?
   and-as-func
   or-as-func
   xor
   xor1           ; Are exactly one of the arguments true?
   ;boolean=?*     ; boolean=, extended for n args, returning a bool.
   implies
   )   

  (define (false? x) (eq? x #f))
  (define (true?  x) (eq? x #t))

  
  (define (and-as-func . args) (and-as-func-list args))
  (define ( or-as-func . args) ( or-as-func-list args))
  (define (and-as-func-list args)
    (cond [(empty? args) #t]
          [(first args) (and-as-func-list (rest args))]
          [else #f]))
  (define ( or-as-func-list args)
    (cond [(empty? args) #f]
          [(first args) (first args)]
          [else (or-as-func-list (rest args))]))

  (define (xor2 a b)
    (if a
        (not b)
        b))
    
  (define (xor . args) (foldl xor2 #f args))
  

  ;; Are exactly 1 of the arguments true?
  (define (xor1 . args)
    (cond [(empty? args) #f]
          [(first args)  (not (apply or-as-func (rest args)))]
          [else (apply xor1 (rest args))]))

  (define (implies a b) (or (not a) b))
     ; todo: Rename this to "implies?"?  
     ; todo: Rewrite as a macro, which doesn't necessarily evaluate b.

  #;(define (boolean=* . args)
    (or (empty? args)
        (apply (if (first args) and-as-func (non or-as-func)) (rest args))))
  )
