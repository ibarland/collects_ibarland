(define-syntax (define/opt stx)
  (syntax-case stx ()
    [(_ (func-name . formals) body)
     (syntax (define func-name (opt-lambda formals body)))]))

(define/opt (foo x y [z 4])
            (+ x y z))
(foo 2 3 4)
(foo 2 3)






(module a mzscheme
  (provide lala)
  (define lala 4))

(module b mzscheme
  (provide lala bala)
  (define lala 'b-wins)
;;;  (require-for-syntax mzscheme)
;  (define-macro (bala x) `(got ,x))
  (define bala 3)
  )

(require b)
(require a)

lala

(define x 3)
(define x 4)
x


(module a mzscheme 
  (provide xm) 
  (define y 2) 
  (define-syntax xm     ; a macro that expands to y
    (syntax-rules () 
      [(xm) y]))) 

(require a)
(xm)


(module c mzscheme
  (provide hala)
  ;(define-macro (hala x) `(define ,x 33))
  (define-syntax hala
    (syntax-rules ()
                  [(hala x) (define x 33)]))
  )
(require c)
(hala w)
w

(require-for-syntax (lib "etc.ss"))
(define-syntax define/opt
  (lambda (stx)
    (syntax-case ()
                  [(define/opt args body)
                   (define (first args) (lambda-opt (rest args) body))])))
(define/opt (foo x y (z 4))
            (+ x y z))
(foo 2 3 4)
(foo 2 3)